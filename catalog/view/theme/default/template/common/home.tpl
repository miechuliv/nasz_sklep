<?php echo $header; ?><?php // echo $column_left; ?>
<div id="content"><?php // echo $content_top; ?>

<div id="home-top">
	<div>
		<div>
			<div>
				<h2><?php echo $this->language->get('text_kategorie_popularne'); ?></h2>
				<ul>
					<li><a href="#">Lorem ipsum lorem</a></li>
					<li><a href="#">Lorem ipsum</a></li>
					<li><a href="#">Ipsum lorem</a></li>
					<li><a href="#">Lorem lorem lorem ipsum</a></li>
					<li><a href="#">Dolos ipsum lorem</a></li>
					<li><a href="#">Ipsum lorem</a></li>
					<li><a href="#">Lorem lorem lorem ipsum</a></li>
				</ul>
			</div>
			<div>
				<h2><?php echo $this->language->get('text_produkty_popularne'); ?></h2>
				<ul>
					<li><a href="#">Lorem ipsum lorem</a></li>
					<li><a href="#">Lorem ipsum</a></li>
					<li><a href="#">Ipsum lorem</a></li>
					<li><a href="#">Lorem lorem lorem ipsum</a></li>
					<li><a href="#">Dolos ipsum lorem</a></li>
					<li><a href="#">Ipsum lorem</a></li>
					<li><a href="#">Lorem lorem lorem ipsum</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div>
		<?php echo $column_left; ?>
	</div>
</div>

<div class="table trzy" id="inform">
	<div>
		<div class="zalety">
			<div>
				<h2><?php echo $this->language->get('home_zalety_h2'); ?></h2>
				<p><?php echo $this->language->get('home_zalety_p'); ?></p>
				<ul><?php echo $this->language->get('home_zalety_ul'); ?></ul>
				<p><?php echo $this->language->get('home_zalety_p2'); ?></p>
			</div>
		</div>
	</div>
	<div>
		<h2><?php echo $this->language->get('home_platnosci_h2'); ?></h2>
		<p><?php echo $this->language->get('home_platnosci_p'); ?></p>
		<div class="table">
			<div><img src="./image/paym/paypal.jpg" alt="paypal"></div>
			<div><img src="./image/paym/master.jpg" alt="master"></div>
			<div><img src="./image/paym/visa.jpg" alt="visa"></div>
		</div>
		<div class="table margin">
			<div><img src="./image/paym/sofort.png" alt="sofort"></div>
			<div><img src="./image/paym/vorkasse.gif" alt="vorkasse"></div>
			<div></div>
		</div>
		<h2><?php echo $this->language->get('home_dostawy_h2'); ?></h2>
		<p><?php echo $this->language->get('home_dostawy_p'); ?></p>
		<div class="table">
			<div><img src="./image/paym/dpd.jpg" alt="ups"></div>
		</div>
	</div>
	<div>
		<h2><?php echo $this->language->get('home_polecamy_h2'); ?></h2>
		<?php echo $column_right; ?>
	</div>
</div>

<div>
	<?php echo $content_top; ?>
</div>

<div id="home-kat">
	<h2><?php echo $this->language->get('text_kategorie'); ?></h2>
	<div>
		<div><div><a href="#"><img src="./image/pen.jpg" alt="kat"/></a><div><span>kategoria lorem ipsum</span></div></div></div>	
		<div><div><a href="#"><img src="./image/pen.jpg" alt="kat"/></a><div><span>kategoria lorem ipsum</span></div></div></div>	
		<div><div><a href="#"><img src="./image/pen.jpg" alt="kat"/></a><div><span>kategoria lorem ipsum</span></div></div></div>	
		<div><div><a href="#"><img src="./image/pen.jpg" alt="kat"/></a><div><span>kategoria lorem ipsum</span></div></div></div>	
		<div><div><a href="#"><img src="./image/pen.jpg" alt="kat"/></a><div><span>kategoria lorem ipsum</span></div></div></div>	
		<div><div><a href="#"><img src="./image/pen.jpg" alt="kat"/></a><div><span>kategoria lorem ipsum</span></div></div></div>	
		<div><div><a href="#"><img src="./image/pen.jpg" alt="kat"/></a><div><span>kategoria lorem ipsum</span></div></div></div>	
		<div><div><a href="#"><img src="./image/pen.jpg" alt="kat"/></a><div><span>kategoria lorem ipsum</span></div></div></div>	
		<div><div><a href="#"><img src="./image/pen.jpg" alt="kat"/></a><div><span>kategoria lorem ipsum</span></div></div></div>	
		<div><div><a href="#"><img src="./image/pen.jpg" alt="kat"/></a><div><span>kategoria lorem ipsum</span></div></div></div>	
		<div><div><a href="#"><img src="./image/pen.jpg" alt="kat"/></a><div><span>kategoria lorem ipsum</span></div></div></div>	
		<div><div><a href="#"><img src="./image/pen.jpg" alt="kat"/></a><div><span>kategoria lorem ipsum</span></div></div></div>	
	</div>
</div>

<div id="home-mid">
	<?php echo $content_bottom; ?>
</div>

<div id="home-kafle">
	<div class="table">
		<div>
			<div>
				<div><a href="#"><img src="./neon.jpg" alt=""/></a></div>
				<div><h3>Lorem ipsum</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vestibulum purus nec nibh viverra, non faucibus dui pretium. Suspendisse eget est ullamcorper, sagittis mauris quis, tincidunt orci. Duis ultrices, quam vel elementum aliquam, metus tortor venenatis dui, facilisis sodales ligula quam ut tellus.</p><a href="#" >Wszystkie produkty z kategorii lorem ipsum <i class="fa fa-angle-right"></i></a></div>
			</div>
		</div>
		<div>
			<div>
				<div><a href="#"><img src="./neon.jpg" alt=""/></a></div>
				<div><h3>Lorem ipsum</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vestibulum purus nec nibh viverra, non faucibus dui pretium. Suspendisse eget est ullamcorper, sagittis mauris quis, tincidunt orci. Duis ultrices, quam vel elementum aliquam, metus tortor venenatis dui, facilisis sodales ligula quam ut tellus.</p><a href="#" >Wszystkie produkty z kategorii lorem ipsum <i class="fa fa-angle-right"></i></a></div>
			</div>
		</div>
		<div>
			<div>
				<div><a href="#"><img src="./neon.jpg" alt=""/></a></div>
				<div><h3>Lorem ipsum</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vestibulum purus nec nibh viverra, non faucibus dui pretium. Suspendisse eget est ullamcorper, sagittis mauris quis, tincidunt orci. Duis ultrices, quam vel elementum aliquam, metus tortor venenatis dui, facilisis sodales ligula quam ut tellus.</p><a href="#" >Wszystkie produkty z kategorii lorem ipsum <i class="fa fa-angle-right"></i></a></div>
			</div>
		</div>		
	</div>
	<div class="table">
		<div>
			<div>
				<div><a href="#"><img src="./neon.jpg" alt=""/></a></div>
				<div><h3>Lorem ipsum</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vestibulum purus nec nibh viverra, non faucibus dui pretium. Suspendisse eget est ullamcorper, sagittis mauris quis, tincidunt orci. Duis ultrices, quam vel elementum aliquam, metus tortor venenatis dui, facilisis sodales ligula quam ut tellus.</p><a href="#" >Wszystkie produkty z kategorii lorem ipsum <i class="fa fa-angle-right"></i></a></div>
			</div>
		</div>
		<div>
			<div>
				<div><a href="#"><img src="./neon.jpg" alt=""/></a></div>
				<div><h3>Lorem ipsum</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vestibulum purus nec nibh viverra, non faucibus dui pretium. Suspendisse eget est ullamcorper, sagittis mauris quis, tincidunt orci. Duis ultrices, quam vel elementum aliquam, metus tortor venenatis dui, facilisis sodales ligula quam ut tellus.</p><a href="#" >Wszystkie produkty z kategorii lorem ipsum <i class="fa fa-angle-right"></i></a></div>
			</div>
		</div>
		<div>
			<div>
				<div><a href="#"><img src="./neon.jpg" alt=""/></a></div>
				<div><h3>Lorem ipsum</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vestibulum purus nec nibh viverra, non faucibus dui pretium. Suspendisse eget est ullamcorper, sagittis mauris quis, tincidunt orci. Duis ultrices, quam vel elementum aliquam, metus tortor venenatis dui, facilisis sodales ligula quam ut tellus.</p><a href="#" >Wszystkie produkty z kategorii lorem ipsum <i class="fa fa-angle-right"></i></a></div>
			</div>
		</div>		
	</div>
	<div class="table">
		<div>
			<div>
				<div><a href="#"><img src="./neon.jpg" alt=""/></a></div>
				<div><h3>Lorem ipsum</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vestibulum purus nec nibh viverra, non faucibus dui pretium. Suspendisse eget est ullamcorper, sagittis mauris quis, tincidunt orci. Duis ultrices, quam vel elementum aliquam, metus tortor venenatis dui, facilisis sodales ligula quam ut tellus.</p><a href="#" >Wszystkie produkty z kategorii lorem ipsum <i class="fa fa-angle-right"></i></a></div>
			</div>
		</div>
		<div>
			<div>
				<div><a href="#"><img src="./neon.jpg" alt=""/></a></div>
				<div><h3>Lorem ipsum</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vestibulum purus nec nibh viverra, non faucibus dui pretium. Suspendisse eget est ullamcorper, sagittis mauris quis, tincidunt orci. Duis ultrices, quam vel elementum aliquam, metus tortor venenatis dui, facilisis sodales ligula quam ut tellus.</p><a href="#" >Wszystkie produkty z kategorii lorem ipsum <i class="fa fa-angle-right"></i></a></div>
			</div>
		</div>
		<div>
			<div>
				<div><a href="#"><img src="./neon.jpg" alt=""/></a></div>
				<div><h3>Lorem ipsum</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vestibulum purus nec nibh viverra, non faucibus dui pretium. Suspendisse eget est ullamcorper, sagittis mauris quis, tincidunt orci. Duis ultrices, quam vel elementum aliquam, metus tortor venenatis dui, facilisis sodales ligula quam ut tellus.</p><a href="#" >Wszystkie produkty z kategorii lorem ipsum <i class="fa fa-angle-right"></i></a></div>
			</div>
		</div>		
	</div>
</div>

</div>

<?php echo $footer; ?>